import React, { createContext, useState } from 'react';
import initalMaintenance from './constants/maintenance'
const InMaintenanceContext = createContext();

function InMaintenanceProvider({ children }) {
  const [inMaintenance, setinMaintenance] = useState(initalMaintenance);
  return (
    <InMaintenanceContext.Provider value={[inMaintenance, setinMaintenance]}>
      { children }
    </InMaintenanceContext.Provider>
  );
}
export {
  InMaintenanceContext,
  InMaintenanceProvider
};
