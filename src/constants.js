module.exports = {
  endpoint: complement => `${process.env.REACT_APP_API}/api/v1/${complement}`,
}
