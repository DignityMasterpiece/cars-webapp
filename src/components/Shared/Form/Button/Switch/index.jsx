import React from 'react';
import './index.css'
const Switch = ({ isOn, handleToggle, disabled = false }) => {
  return (
    <div id="form-button-switch" >
      <input
        checked={isOn}
        disabled={disabled}
        onChange={handleToggle}
        className="react-switch-checkbox"
        id={`react-switch-new`}
        type="checkbox"
      />
      <label
        className="react-switch-label"
        htmlFor={`react-switch-new`}
        style={{ background: isOn && '#06D6A0' }}
      >
        <span className={`react-switch-button`} />
      </label>
    </div>
  );
};

export default Switch;