import React, { useContext } from 'react';
import moment from 'moment-timezone'
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import CarService from '../../../../services/CarService';
import { InMaintenanceContext } from '../../../../context/inMaintenance';
import initalMaintenance from '../../../../context/constants/maintenance'

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: '30%',
    left: '60%',
    transform: `translate(-50%, -50%)`,
  },
  form: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
      display: 'flex',
      flexWrap: 'wrap',
    },
  },
  textField: {

  },
  button: {
    width: '100%'
  }
}));

export default function Maintenance({ callBack }) {
  const classes = useStyles();
  const [inMaintenanceInfo, setInMaintenance] = useContext(InMaintenanceContext);
  const { open, make, model, id, estimateDate, description, owner } = inMaintenanceInfo;
  const handleChange = event => {
    const { target: { value, id } } = event
    setInMaintenance(inMaintenanceInfo => ({
      ...inMaintenanceInfo,
      [id]: value.trim()
    }))
  }
  const handleValidation = () => {
    let errors = [];
    let isValid = true;
    if (!Number.isInteger(id) || id === undefined)
      errors.push('El id debe ser un entero y es requerido.');
    if (!moment(estimateDate, 'YYYY/MM/DD', true).isValid() || estimateDate === undefined || estimateDate.trim() === '')
      errors.push('La fecha estimada debe ser un texto con formato "YYYY/MM/DD" y es requerido.');
    if (typeof description !== 'string' || description === undefined || description.trim() === '')
      errors.push('La descripcion debe ser un texto y es requerido.');
    if (typeof owner !== 'string' || owner === undefined || owner.trim() === '')
      errors.push('El nombre del propietario debe ser un texto y es requerido.');
    if (errors.length)
      isValid = false;
    return {
      errors,
      isValid
    }
  }
  const handleClose = () => {
    setInMaintenance(inMaintenanceInfo => (initalMaintenance));
    callBack()
  }

  const saveRegistry = async event => {
    try {
      const { isValid, errors } = handleValidation();
      if (isValid) {
        const response = await CarService.maintenance({
          id,
          description,
          owner,
          estimateDate
        });
        const { success, message } = response;
        if(success){
          alert(message);
          handleClose();
        }
      } else {
        alert(errors.join('\n'))
      } 
    } catch (error) {
      alert(error.message || error)
      throw error
    }
  }
  const body = (
    <div className={classes.paper}>
      <h2 id="simple-modal-title">Mantenimiento</h2>
      <form className={classes.root} noValidate autoComplete="off">
        <div>
          <TextField
            disabled
            id="make"
            label="Marca"
            defaultValue={make}
            className={classes.textField}
            placeholder="Marca"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            disabled
            id="model"
            label="Modelo"
            defaultValue={model}
            className={classes.textField}
            placeholder="Modelo"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="estimateDate"
            label="Fecha Estimada"
            defaultValue={estimateDate}
            className={classes.textField}
            onChange={handleChange}
            placeholder="Fecha Estimada"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="description"
            label="Descripción"
            defaultValue={description}
            className={classes.textField}
            onChange={handleChange}
            placeholder="Motivo de mantenimiento"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="owner"
            label="Propietario"
            defaultValue={owner}
            className={classes.textField}
            onChange={handleChange}
            placeholder="Nombre del propietario"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            endIcon={<SaveIcon />}
            onClick={saveRegistry}
          >
            Registrar
          </Button>
        </div>
      </form>
    </div>
  );

  return (
    <Modal
      open={open}
      onClose={handleClose}
    >
      {body}
    </Modal>
  );
}