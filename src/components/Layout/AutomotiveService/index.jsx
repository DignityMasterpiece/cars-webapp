import React, { useEffect, useState } from 'react'
import CarService from '../../../services/CarService'
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import CarCard from './CarCard'
import Maintenance from './Maintenance'
import { InMaintenanceProvider } from '../../../context/inMaintenance'

export default function AutomotiveService() {
  const [cars, setCars] = useState([]);
  const [renderFlag, setRenderFlag] = useState(false);
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: '#ffc305e0',
      padding: '24px',
      maxWidth: '100%',
    },
    content: {
      '& > *': {
        margin: theme.spacing(1),
      },
      overflowY: 'auto',
      display: 'flex',
      height: '100%',
      flexWrap: 'wrap',
      flexDirection: 'row'
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
    },
  }));

  const classes = useStyles();

  const getInformation = async () => {
    const { result } = await CarService.get();
    setCars(result)
  }

  useEffect(() => {
    getInformation();
  }, [renderFlag])
  
  const refresh = () => {
    setRenderFlag(!renderFlag)
  }

  const populateCars = () => cars.map((car, idx) => <CarCard key={idx} information={car} />)
  
  return (
    <InMaintenanceProvider>
      <Container className={classes.root} maxWidth='lg'>
        <Paper
          className={classes.content}
          elevation={15}
          variant="outlined">
          {populateCars()}
          <Maintenance 
            callBack={refresh}
          />
        </Paper>
      </Container>
    </InMaintenanceProvider>
  )
};