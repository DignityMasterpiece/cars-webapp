import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import SwitchButton from '../../../Shared/Form/Button/Switch';
import { InMaintenanceContext } from '../../../../context/inMaintenance';

const useStyles = makeStyles({
  root: {
    width: 400,
    height: 350,
    textAlign: 'center'
  },
  media: {
    height: 140,
  },
  maintenance:{
    margin: '0 auto'
  }
});

export default function CarCard({ information }) {
  const { id, make, model, estimateDate, image, description, inMaintenance } = information;
  const [inMaintenanceInformation, setInMaintenance] = useContext(InMaintenanceContext)
  const classes = useStyles();

  const populateManintenance = information => {
    setInMaintenance(inMaintenanceInformation => ({
      ...inMaintenanceInformation,
      ...information,
      open: true
    }))
  }
  
  return (
    <Card key={id} className={classes.root}>
      <Tooltip title={inMaintenance? 'En mantenimiento': 'Registrar mantenimiento'}>
        <CardActionArea
          onClick={e => populateManintenance(information)}
        >
          <CardMedia
            className={classes.media}
            image={image}
            title={model}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {`${model} - ${make}`}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {description}
              <br></br>
              {estimateDate||'sin fecha'}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Tooltip>
      <CardActions>
        <FormLabel className={classes.maintenance} size="small" color="primary">
          Mantenimiento
          <SwitchButton
            isOn={inMaintenance}
            handleToggle={null}
            disabled={true}
          />
        </FormLabel>
      </CardActions>
    </Card>
  );
}