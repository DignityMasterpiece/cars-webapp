import React from 'react'
import SideBar from './Sidebar'
import AutomotiveService from './AutomotiveService'
export default function Layout() {
  return (
    <main id='main-app'>
      <SideBar />
      <AutomotiveService/>
    </main>
  )
};