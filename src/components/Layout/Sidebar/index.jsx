import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import DriveEtaIcon from '@material-ui/icons/DriveEta';
const Index = () => {
  const useStyles = makeStyles((theme) => ({
    sideBar: {
      width: '100%',
      background:'#222',
      height: '100vh',
      maxWidth: 360,
      paddingTop: '0px',
      paddingBottom: '0px',
    },
    icon: {
      cursor: 'pointer',
      color:'#fff'
    }
  }));
  const classes = useStyles();
  return (
    <List className={classes.sideBar}>
      <ListItem>
        <ListItemAvatar>
          <Avatar className={classes.icon}>
            <DriveEtaIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Cars" secondary="Demo app." />
      </ListItem>
    </List>
  );
}
export default Index;

