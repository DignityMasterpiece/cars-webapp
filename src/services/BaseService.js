import axios from 'axios';
class BaseService {
  constructor(headers) {
    let service = axios.create({
      crossdomain: true,
      headers: {
        ...headers,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    });
    this.service = service;
  }

  fetch = (url, data) => {
    data.url = url;
    return this.service(data)
      .then(response => {
        return this.handleResponse(response);
      })
      .catch(e => {
        return this.handleReject(e);
      });
  };

  get(path) {
    return this.fetch(path, {
      method: "GET"
    });
  }

  patch(path, payload) {
    return this.fetch(path, {
      method: "PATCH",
      data: payload
    });
  }

  post(path, payload) {
    return this.fetch(path, {
      method: "POST",
      data: payload
    });
  }

  delete(path, payload) {
    return this.fetch(path, {
      method: "DELETE",
      data: payload
    });
  }

  put(path, payload) {
    return this.fetch(path, {
      method: "PUT",
      data: payload
    });
  }

  handleResponse(response) {
    console.log(response);
    if (response.status === 200) {
      return Promise.resolve(response.data);
    }
  }

  handleReject(err) {
    if (!err.response) {
      Promise.reject(err)
    } else {
      console.log('error de respuesta', err.response);
      return Promise.reject(err.response.data);
    }
  }
}

export default BaseService;
