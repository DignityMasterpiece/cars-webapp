import BaseService from "./BaseService";
import { endpoint } from "../constants";

const CarService = {
  get: () => {
    const axios = new BaseService();
    return axios.get(endpoint('car'));
  },
  maintenance: payload => {
    const axios = new BaseService();
    return axios.patch(endpoint('car/maintenance'), payload);
  },
};

export default CarService;
